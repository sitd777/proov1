
CREATE TABLE `students` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `students` ADD PRIMARY KEY (`id`);
ALTER TABLE `students` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

INSERT INTO `students` (`id`, `name`) VALUES
(1, 'Alice'),
(2, 'Bob'),
(3, 'Max');

CREATE TABLE `subjects` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `subjects` ADD PRIMARY KEY (`id`);
ALTER TABLE `subjects` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

INSERT INTO `subjects` (`id`, `name`) VALUES
(1, 'Matemaatika'),
(2, 'Füüsika'),
(3, 'Hiina keel'),
(4, 'Bioloogia'),
(5, 'Informaatika');

CREATE TABLE `students_subjects` (
  `student_id` int(11) UNSIGNED DEFAULT NULL,
  `subject_id` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `students_subjects` ADD UNIQUE( `student_id`, `subject_id`);
ALTER TABLE `students_subjects`
  ADD KEY `idx-students_subjects-student_id` (`student_id`),
  ADD KEY `idx-students_subjects-subject_id` (`subject_id`);

ALTER TABLE `students_subjects`
  ADD CONSTRAINT `fk-students_subjects-student_id` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk-students_subjects-subject_id` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO `students_subjects` (`student_id`, `subject_id`) VALUES
(1, 1),
(1, 2),
(2, 2),
(2, 3),
(2, 4),
(3, 5);



