<?php

$link = mysqli_connect('localhost', 'root', '');
mysqli_select_db($link, 'prv1');
mysqli_query($link, "SET NAMES utf8");

$filterStudent = !empty($_REQUEST['student']) ? (int)$_REQUEST['student'] : 0;
$filterSubject = !empty($_REQUEST['subject']) ? (int)$_REQUEST['subject'] : 0;

// Get students for filter
$studentsList = [];
$qry = mysqli_query($link, "SELECT * FROM `students`");
while($arr = mysqli_fetch_assoc($qry)) { $studentsList[$arr['id']] = $arr['name']; }

// Get subjects for filter
$subjectsList = [];
$qry = mysqli_query($link, "SELECT * FROM `subjects`");
echo mysqli_error($link);
while($arr = mysqli_fetch_assoc($qry)) { $subjectsList[$arr['id']] = $arr['name']; }

// Get data for table
$sql = "
    SELECT 
      st.name studentName,
      GROUP_CONCAT(sb.`name` SEPARATOR ', ') subjects
    FROM 
      `students` st
    LEFT JOIN
      `students_subjects` ss
    ON 
      st.`id` = ss.`student_id`
    LEFT JOIN
      `subjects` sb
    ON 
      ss.`subject_id` = sb.`id`
    WHERE 
      1
    ";
if($filterStudent) $sql .= " AND st.`id` = " . $filterStudent;
if($filterSubject) $sql .= " AND sb.`id` = " . $filterSubject;
$sql .= " GROUP BY st.`id` ";
$qry = mysqli_query($link, $sql);
?>

<!DOCTYPE html>
<html lang="et-EE">
<head>
    <meta charset="UTF-8">
    <title>Proovitöö</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"/>
    <link rel="stylesheet" href="css/style.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Provitöö</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Students</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container marginTop">
    <form method="post">
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-3">
                <select name="student">
                    <option value="0">---</option>
                    <?php foreach ($studentsList as $studentId => $studentName) { ?>
                        <option value="<?= $studentId ?>"<?= $studentId == $filterStudent ? ' selected="true"' : '' ?>><?= $studentName ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-3">
                <select name="subject">
                    <option value="0">---</option>
                    <?php foreach ($subjectsList as $subjectId => $subjectName) { ?>
                        <option value="<?= $subjectId ?>"<?= $subjectId == $filterSubject ? ' selected="true"' : '' ?>><?= $subjectName ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-primary">Filter</button>
            </div>
        </div>
    </div>
    </form>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Student</th>
                <th>Subjects</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            <?php while($arr = mysqli_fetch_assoc($qry)) { ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $arr['studentName'] ?></td>
                    <td><?= $arr['subjects'] ?></td>
                </tr>
                <?php $i++ ?>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>
